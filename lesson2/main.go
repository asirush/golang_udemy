package main

import (
	"errors"
	"fmt"
	"log"
)

func main() {
	print()
	print()
	print()
	printMessage("Message printed from a function")
	message := sayHello("Gopher", 20)
	printMessage(message)

	message, adult := checkAge(20)
	message2, _ := checkAge(15) //ignore the second return value
	fmt.Print(message, " ", adult, "\n")
	fmt.Print(message2, "\n")

	// Error handling
	err := getError(false)
	if err != nil {
		log.Fatal(err)
	}
}

func print() {
	fmt.Println("Hello, World!")
}

func printMessage(message string) {
	fmt.Println(message)
}

func sayHello(name string, age int) string {
	//return "Hello, " + name + "!" + " " + "You are " + string(age) + " years old."
	return fmt.Sprintf("Hello, %s! You are %d years old.", name, age)
}

func checkAge(age int) (string, bool) {
	if age >= 18 && age < 45 {
		responce := "You are an adult"
		return responce, true
	} else if age >= 45 && age < 60 {
		return "You are a middle aged", true
	} else {
		responce := "You are a minor"
		return responce, false
	}

	/*
		if age >= 18 {
			responce := "You are an adult"
			return responce, true
		}
		return "You are not adult", false
	*/
}

func getError(state bool) error {
	if state {
		return nil // nil means no error
	} else {
		return errors.New("Check the state of the function. It is false.")
	}
}
