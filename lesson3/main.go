package main

func main() {
	prediction("Monday")
}

func prediction(dayOfWeek string) {
	switch dayOfWeek {
	case "Monday": // Monday
		println("Today is Monday")
	case "Tuesday": // Tuesday
		println("Today is Tuesday")
	}
}
