package main

import (
	"fmt"
	"reflect"
)

func main() {
	const message2 string = "Hello, Message 2!" // init message2 variable
	var message3 string = "Hello, Message 3!"   // init message3 variable
	message := "Hello, World!"                  // init message variable
	message = "Hello, Go!"                      // reassign message variable
	fmt.Println(message + " " + message2 + " " + message3)
	fmt.Println(reflect.TypeOf(message)) // print type of message

	runesSlice := []byte("abc") // convert string to byte slice
	var a byte = 97             // convert int to byte
	var b rune = 'b'            // convert string to rune
	fmt.Println(runesSlice)
	fmt.Println(a)
	fmt.Println(b)

	e, f, g := 1, 2, 3 // multiple variable declaration
	e, f = f, e        // swap variables
	fmt.Println(e, f, g)
}
